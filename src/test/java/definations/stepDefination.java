package definations;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.callingDriver;
import org.openqa.selenium.WebDriver;
import pages.loginPage;
import helper.readProperty;

public class stepDefination extends callingDriver {
    WebDriver Driver;
    loginPage LoginPage = new loginPage();
    readProperty read = new readProperty();

@Before
  public void initiateDriver(){
       Driver = createDriver();
      LoginPage.driverCalling(Driver);

    }

    @Given("^User hit the url \"([^\"]*)\"$")
    public void user_hit_the_url(String env) throws Throwable {
        String Url = read.readEnv(env,"url");
        LoginPage.hittingURL(Url);


    }

    @When("^User see the title \"([^\"]*)\"$")
    public void user_see_the_title(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^User should click the  \"([^\"]*)\"$")
    public void user_should_click_the(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^User should see the link \"([^\"]*)\"$")
    public void user_should_see_the_link(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @After
    public  void killingDriver(){
    Driver.quit();
    }



}
