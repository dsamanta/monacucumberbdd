package helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class readProperty {

    String propertyValue;


    public String readEnv(String env, String value) throws FileNotFoundException {
        String fileName = "./testData/env_" + env + ".properties";

        InputStream input = new FileInputStream(fileName);
        Properties prop = new Properties();
        try {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertyValue = prop.getProperty(value);
    }
}
